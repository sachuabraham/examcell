
<html><head>
      <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Registration</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
      <link href="assets/css/style.css" rel="stylesheet">
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet">
     <!-- GOOGLE FONTS-->
   <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
</head>
<body>
 <?php/*
  require('database.php');
  session_start();
  $name=$_SESSION['name'];*/
  ?>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Administrator</a>
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> Last access : 30 May 2014 &nbsp; <a href="login.html" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="assets/img/find_user.png" class="user-image img-responsive">
					</li>



                              <li>
                                  <a class="active-menu" href="basic.html"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                              </li>
                                <li>
                                  <a href="timetable.html"><i class="fa fa-table fa-3x"></i> Time Table</a>
                              </li>
                               <li>
                                  <a href="#"><i class="fa fa-bar-chart-o fa-3x"></i> Upload Portal<span class="fa arrow"></span></a>
                                  <ul class="nav nav-second-level">
                                      <li>
                                          <a href="upload.html">Semester 1</a>
                                      </li>
                                      <li>
                                          <a href="upload.html">Semester 2</a>
                                      </li>
                                      <li>
                                          <a href="upload.html">Semester 3</a>
                                      </li>
                                      <li>
                                          <a href="upload.html">Semester 4</a>
                                      </li>
                                      <li>
                                          <a href="upload.html">Semester 5</a>
                                      </li>
                                      <li>
                                          <a href="upload.html">Semester 6</a>
                                      </li>
                                      <li>
                                          <a href="upload.html">Semester 7</a>
                                      </li>
                                      <li>
                                          <a href="upload.html">Semester 8</a>
                                      </li>
                       </ul>
                              </li>
                               <li>
                                  <a href="class.html"><i class="fa fa-qrcode fa-3x"></i> Class Selection</a>
                              </li>
                                <li>
                                  <a href="staff.html"><i class="fa fa-desktop fa-3x"></i> Staff Selection</a>
                              </li>
                               <li>
                                  <a href="seating.html"><i class="fa fa-edit fa-3x"></i> Gen. Seating Arrangment</a>
                              </li>


                              <li>
                                  <a href="#"><i class="fa fa-sitemap fa-3x"></i> Database Management<span class="fa arrow"></span></a>
                                  <ul class="nav nav-second-level">
                                      <li>
                                          <a href="#">Faculty Database<span class="fa arrow"></span></a>
                                          <ul class="nav nav-third-level">
                                              <li>
                                                  <a href="upfac.php">Update Faculty</a>
                                              </li>
                                              <li>
                                                  <a href="delfac.php">Delete Faculty</a>
                                              </li>
                                              <li>
                                                  <a href="mfac.html">Modify Faculty</a>
                                              </li>

                                          </ul>
                                      </li>
                                      <li>
                                          <a href="#">Student Database<span class="fa arrow"></span></a>
                                          <ul class="nav nav-third-level">
                                              <li>
                                                  <a href="upstu.php">Update Student</a>
                                              </li>
                                              <li>
                                                  <a href="upstu.php">Delete Student</a>
                                              </li>
                                              <li>
                                                  <a href="mstu.html">Modify Student</a>
                                              </li>

                                          </ul>
                                      </li>
                                      <li>
                                          <a href="#">Exam Hall Database<span class="fa arrow"></span></a>
                                          <ul class="nav nav-third-level">
                                              <li>
                                                  <a href="upexam.php">Update Exam Hall</a>
                                              </li>
                                              <li>
                                                  <a href="delexam.php">Delete Exam Hall</a>
                                              </li>
                                              <li>
                                                  <a href="mexam.html">Modify Exam Hall</a>
                                              </li>

                                          </ul>

                                      </li>
                                  </ul>
                      </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12 ">
                     <h2 class="und">User registration</h2>
                     <hr>
                        <br><br>

                        <form class="form-horizontal box" action="user.php" method="post">
                           <div class="form-group">
                             <label class="control-label col-sm-2" for="stid">Staff Id:</label>
                                <div class="col-sm-10">
                                  <input class="form-control" name="stid" placeholder="Enter Staff Id" type="text">
                                  <br>
                                </div>
                                <label class="control-label col-sm-2" for="uname">User Name</label>
                                   <div class="col-sm-10">
                                     <input class="form-control" name="uname" placeholder="Enter User Name" type="text">
                                     <br>
                                   </div>
                                <label class="control-label col-sm-2" for="pass">Password</label>
                                   <div class="col-sm-10">
                                     <input class="form-control" name="pass" placeholder="Enter Password" type="password">
                                     <br>
                                   </div>

                                   <label class="control-label col-sm-2" for="rpass">Repeat Password</label>


                                         <div class="col-sm-10">
                                           <input class="form-control" name="rpass" placeholder="Repeat Password" type="password">
                                           <br>
                                         </div>


                        <br><br>
                        <div class="col-sm-offset-2 col-sm-10">
                                <input name="sub" class="btn btn-default" type="submit">
                              </div>
                        </div>
                        </form>
                        <?php
                        if(isset($_POST['sub']))
                        {
                         require('database.php');

                        $servername = "localhost";
                        $username = "sam";
                        $password = "";

                       // Create connection

                        $fid=$_POST["stid"];
                        $fid=rtrim(ltrim($fid));

                        $uname=$_POST['uname'];
                        $pass =$_POST['pass'];
                        $rpass=$_POST['rpass'];

                        if ($t=$db->query("SELECT * FROM teacher WHERE id='$fid'"))
                          {
                                    $row = $t->fetch_assoc();
                                    $name=$row['name'];
                                    $desig= $row['desig'];
                                    $dept=$row['dept'];
                                    if(strcmp($pass,$rpass)!=0)
                                    {
                                        echo "password not same\n";
                                    }
                                    else
                                    {
                                        $sql="insert into login values('$name','$dept','$desig','$uname','$pass')";
                                        if ($t=$db->query($sql))
                                        {
                                            echo "<br>.insertion successfull\n";
                                        }
                                        else
                                        {
                                            echo "insertion not successfull\n";
                                        }
                                    }
                        }
                            else {
                               echo "Staff id not in database" ;
                           }




                          }
                            ?>
                             </div>
 </div>

    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>




</body></html>
